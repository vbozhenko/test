<?php

class Point
{
    public $lat;
    public $long;

    function __construct($lat, $long)
    {
        $this->lat = $lat;
        $this->long = $long;
    }
}

function getGeoZoneCenter($markers)
{
    $lat = $lon = 0.0;
    $total = count($markers);
    foreach ($markers as $marker) {
        $lon += $marker[0];
        $lat += $marker[1];
    }
    $centerLat = $lat / $total;
    $centerLon = $lon / $total;

    return json_encode([$centerLon, $centerLat]);
}

function pointInPolygon($p, $polygon)
{
    //if (hundred)thousands of points
    set_time_limit(60);
    $c = 0;
    $p1 = $polygon[0];
    $n = count($polygon);

    for ($i = 1; $i <= $n; $i++) {
        $p2 = $polygon[$i % $n];
        if ($p->long > min($p1->long, $p2->long)
            && $p->long <= max($p1->long, $p2->long)
            && $p->lat <= max($p1->lat, $p2->lat)
            && $p1->long != $p2->long) {
            $xinters = ($p->long - $p1->long) * ($p2->lat - $p1->lat) / ($p2->long - $p1->long) + $p1->lat;
            if ($p1->lat == $p2->lat || $p->lat <= $xinters) {
                $c++;
            }
        }
        $p1 = $p2;
    }

    // if the number of edges we passed through is even, then it's not in the poly.
    return $c % 2 != 0;
}
