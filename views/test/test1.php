<?php

require \Yii::getAlias('@app') . '/components/utils.php';
require \Yii::getAlias('@app') . '/data/numbers.php';

use GuzzleHttp\Promise;

$xml = new \SimpleXMLElement(file_get_contents(\Yii::getAlias('@app') . '/data/geozone.kml'));
$coordinatesString = (string)$xml->Document->Placemark->Polygon->outerBoundaryIs->LinearRing->coordinates;
$coordinatesArray = preg_split('/\s+/', $coordinatesString, -1, PREG_SPLIT_NO_EMPTY);
$geozoneCoordinates = $longitudes = $latitudes = $polygonPoints = [];

foreach ($coordinatesArray as $latLong) {
    $coords = explode(",", $latLong);
    $latitudes[] = $coords[0];
    $longitudes[] = $coords[1];
    $polygonPoints[] = new Point($coords[1], $coords[0]);
    $geozoneCoordinates[] = [$coords[1], $coords[0]];
}

$geoZoneCenter = getGeoZoneCenter($geozoneCoordinates);
$centers = getKadastCenters($numbers);
$kadastrs = checkForInPolygon($centers, $polygonPoints);

function getKadastCenters($numbers)
{
    $client = new GuzzleHttp\Client();
    foreach ($numbers as $number) {
        $promises[$number] = $client->getAsync('https://soft.farm/api/open/cadastral/find-center-by-cadastral-number',
            ['query' => [
                'clientId' => 'LMRZYHROM1o94YQQXMs42-P3s-n6thKQVu9EjS17SCX3nnqT151Ile035wvCi7hC1E33fzxUSJuYnHgB',
                'cadastralNumber' => $number
            ]]);
    }
    $responses = Promise\Utils::settle($promises)->wait();

    $centers = [];
    foreach ($responses as $number => $response) {
        if ($response['value']->getStatusCode() === 200) {
            $result = json_decode($response['value']->getBody(), true);
            if ($result['data'])
                $centers[$number] = $result['data'];
        }
    }

    return $centers;
}

function checkForInPolygon($centers, $polygon)
{
    $inPolygon = $notInPolygon = [];
    foreach ($centers as $code => $center) {
        $point = new Point($center['lat'], $center['lng']);
        if (pointInPolygon($point, $polygon))
            $inPolygon[$code] = $center['lat'] . ',' . $center['lng'];
        else
            $notInPolygon[$code] = $center['lat'] . ',' . $center['lng'];
    }

    return [$inPolygon, $notInPolygon];
}

?>

<div>
    <div class="row">
        <div class="col-xs-3 col-md-3">
            <nav class="flex-column nav-numbers" style="height: 795px;overflow-y:scroll;">
                <span class="label" style="padding: 0.5rem 1rem;">Попадают в геозону:</span>
                <?php
                foreach ($kadastrs[0] as $number => $coordinates)
                    echo '<a class="nav-link" href="#" onclick="showPointer(this)" data-id="' . $coordinates . '">' . $number . '</a>';
                ?>
                <hr>
                <span class="label" style="padding: 0.5rem 1rem;">Не попадают в геозону:</span>'
                <?php
                foreach ($kadastrs[1] as $number => $coordinates)
                    echo '<a class="nav-link" href="#" onclick="showPointer(this)" data-id="' . $coordinates . '">' . $number . '</a>';
                ?>
            </nav>
        </div>
        <div class="col-xs-9 col-md-9">
            <div id="map" style="width: 100%; height: 795px;"></div>
        </div>
    </div>
</div>

<script>
    var geozoneCoordinates = <?= json_encode($geozoneCoordinates) ?>;
    var geoZoneCenter = <?= $geoZoneCenter ?>;
</script>

<?php include 'map.php' ?>
