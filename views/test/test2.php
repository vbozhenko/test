<?php

require \Yii::getAlias('@app') . '/components/utils.php';

use Symfony\Component\Process\Process;

if (!file_exists(\Yii::getAlias('@app') . '/data/polygon.geojson'))
    convertShapeToJson();

$contents = file_get_contents(\Yii::getAlias('@app') . '/data/polygon.geojson');
$decoded = json_decode($contents, true);
$coordinatesArray = $decoded['features'][0]['geometry']['coordinates'][0];
foreach ($coordinatesArray as $latLong) {
    $geozonePolygon[] = [(string)$latLong[1], (string)$latLong[0]];
}

$contents = file_get_contents(\Yii::getAlias('@app') . '/data/boundary.geojson');
$decoded = json_decode($contents, true);
$coordinatesArray = $decoded['features'][0]['geometry']['coordinates'][0][0];

foreach ($coordinatesArray as $latLong) {
    $boundaryPolygon[] = [$latLong[1], $latLong[0]];
}

$geoZoneCenter = getGeoZoneCenter($boundaryPolygon);
$inPolygon = checkForInPolygon($geozonePolygon, $boundaryPolygon);

function convertShapeToJson()
{
    $process = new Process('ogr2ogr -f GeoJSON /data/polygon.geojson /data/form_polygon.shp');
    $process->run();
}

function checkForInPolygon($geozonePolygon, $boundaryPolygon)
{
    $boundaryPoints = [];
    foreach ($boundaryPolygon as $latLong)
        $boundaryPoints[] = new Point($latLong[0], $latLong[1]);

    foreach ($geozonePolygon as $coords) {
        $point = new Point($coords[0], $coords[1]);
        if (!pointInPolygon($point, $boundaryPoints))
            return false;
    }

    return true;
}

?>

<div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div id="map" style="width: 100%; height: 795px;"></div>
        </div>
    </div>
</div>

<script>
    var geoZoneCenter = <?= $geoZoneCenter ?>;
    var boundaryCoordinates = <?= json_encode($boundaryPolygon); ?>;
    <?php if ($inPolygon) : ?>
        var innerCoordinates = <?= json_encode($geozonePolygon); ?>;
    <?php endif ?>
</script>

<?php include 'map.php' ?>


