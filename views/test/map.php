<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
      integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
      crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>

<script>
    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
    var mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    var cadastrUrl = 'http://map.land.gov.ua/geowebcache/service/wms';

    var satellite = L.tileLayer(mbUrl, {
        id: 'mapbox/satellite-v9',
        tileSize: 512,
        zoomOffset: -1,
        attribution: mbAttr
    });

    var streets = L.tileLayer(mbUrl, {
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        attribution: mbAttr
    });

    var kadastr = L.tileLayer.wms(cadastrUrl, {
        layers: 'kadastr',
        format: 'image/png'
    });

    var map = L.map('map', {
        center: geoZoneCenter,
        zoom: 15,
        layers: [streets, satellite]
    });

    var baseMaps = {
        "Streets": streets,
        "Satellite": satellite,
    };

    var overlayMaps = {
        "Kadastr": kadastr
    };

    L.control.layers(baseMaps, overlayMaps).addTo(map);
    map.addLayer(kadastr);

    if (typeof geozoneCoordinates !== 'undefined')
        var polygon = L.polygon(geozoneCoordinates).addTo(map);

    if (typeof boundaryCoordinates !== 'undefined')
        var boundaryPolygon = L.polygon(boundaryCoordinates).setStyle({
            color: 'red'
        }).addTo(map);

    if (typeof innerCoordinates !== 'undefined')
        var innerPolygon = L.polygon(innerCoordinates).addTo(map);

    function showPointer(e) {
        var coords = e.getAttribute('data-id');
        if (coords !== undefined) {
            coords = coords.split(',');
            map.panTo(coords);
            L.marker(coords).addTo(map);
        }
    }

</script>